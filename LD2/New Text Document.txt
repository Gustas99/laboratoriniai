/*
  Mikrovaldikliu sistemos
  2 lab darbas. 
  Variantas 1
*/

// Declare library
#include <avr/io.h> 
#include <util/delay.h>
#include <avr/interrupt.h>

  // Globalus kintamieji 
int status=0; // busenos saugojimo kintamasis
int number=0; // duomenu skaiciavimo kintamasis
int i=2; // LED diodo sekos isvado kintamasis
int data[8]={0b000000, 0b000001, 0b000010, 0b000011, 0b000100, 0b000101, 0b000110, 0b000111}; 

//---------------------------------------------
  //Isorines pertrauktys 
ISR(INT1_vect) 
{
  if(number < 7) 
  { 
    number=number+1; // didinama vienetu
  } 
  else 
  {
    number=0; // numetama reiksme i nuli 
  }
  PORTC=data[number];
}

ISR(PCINT2_vect) 
{
  if(PIND&(1<<PD4))//Stebima PD4 išvado signalo lygis
  {
    //Pin isvado itampos keitimo lygis is zemo i auksta
    if(status==0) 
    {
      status=1;
    } 
    else 
    {
      status=0;
    }  
  } 
 
}
//-----------------------------------------
 // Program starts from main fuction
int main(void){ 
  //Nustatome isvadus PB5-PB2 kaip isėjimo 
  DDRB|=(1<<PB5)|(1<<PB4)|(1<<PB3)|(1<<PB2);
  DDRB&=~(1<<PB0); // Nustatomas isvadas PB0 kaip iejimo 
  PORTB|=(1<<PB0); //Nustatomas pull-up rezistorius PB0
  //Nustatome isvadus PC5-PC0 kaip isejimo
  DDRC|=(1<<PC5)|(1<<PC4)|(1<<PC3)|(1<<PC2)|(1<<PC1)|(1<<PC0);
  
  //--------------------------------------------
  // Sukonfiguruojame isorine pertraukti INT1
  //Savo nuoziura nustatome isvada PD3 kaip iejimo
  DDRD&=~(1<<PD3); 
  PORTD|=(1<<PD3); //Nustatomas pull-up rezistorius PD3
  //Nustatomas kylantis frontas isorinei pertraukciai INT1
  EICRA|=(1<<ISC11)|(1<<ISC10); 
  //Nustatomas isoriniu pertraukciu kaukes registras
  EIMSK|=(1<<INT1); 
  
  //--------------------------------------------
  // PCINT - PIN isvadu keitimo pertrauktis
  //PIN CHANGE INTERRUPTS code example 
  // PCINT20-PD4 
  //Savo nuoziura nustatome isvada PD4 kaip iejimo
  DDRD&=~(1<<PD4);
  PORTD|=(1<<PD4); //Nustatomas pull-up rezistorius PD4
  //Nustatome PCIE2, kad galetume nuskaityti PCMSK2 kauke
  PCICR|=(1<<PCIE2); 
  //Nustatau PCINT20, kad suaktyvinciau busenos keitimo pertraukti
  PCMSK2|=(1<<PCINT20);
  
  //--------------------------------------------
  //Komanda visoms pertrauktims 
  SREG|=(1<<7); // igalina visu SREG  7 bitu pertraukima 
 
  //--------------------------------------------
  while(1){   // begin f endless while cycle
    //Pooling metodas. 
     if(!(PINB&(1<<PB0))) //Stebima PB0 išvado signalo lygis 
     { //Kai mygtukas paspaustas
        PORTB|=(1<<PB5); // LED diodas sviecia
     }
      else 
      {
        //Kai mygtukas nepaspaustas
        PORTB&=~(1<<PB5); // LED diodas nesviecia 
      } 
    //Atvaizduojame reiksmes nuo 0 iki 7 dvejetaineje sistemoje
    //and raudonu LED diodu
    PORTC=data[number];
    
    PORTB&=~(1<<i); // isvado priskyrimas PB2-PB4
    
    if(status == 0) // mygtuko busenos lygis
    {
      if(i<4) // tikrinamas LED diodo isvado nr.
      {
        i=i+1; // didinama vienetu
      } 
      else 
      {
        i=2; // numetame reiksme i 2
      }
    }
    
    PORTB|=(1<<i); // Nustatomas pull up rezistorius isvadams PB2-PB4
    
    _delay_ms(250); // velinimas 0.25s
	} // end of endless while cycle
  
  
}
