/*
  Mikrovaldikliu sistemos
  6 lab darbas. 
  Variantas 1
*/


#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include<stdio.h> 
#include<stdlib.h>
#include<string.h>

char buf[50]; // char tipo masyvas temperaturos vertems atvaizduoti
double temp=0; // pradine temperaturos verte


#define LCD_Port         PORTD	// Apibreziamas LCD prievadas (PORTA, PORTB, PORTC, PORTD)
#define LCD_DPin         DDRD	// Apibreziami 4-bitu isvadai (PD4-PD7 at PORT D)
#define LCD_DATA0_PIN    PD4    // < pin for 4bit data bit 0  
#define LCD_DATA1_PIN    PD5    // < pin for 4bit data bit 1  
#define LCD_DATA2_PIN    PD6    // < pin for 4bit data bit 2  
#define LCD_DATA3_PIN    PD7    // < pin for 4bit data bit 3  
#define RSPIN 			 PD3	// RS Pin
#define ENPIN            PD2 	// E Pin
int runtime;			 // Laiktrodis LCD ekranui

void LCD_Init (void)
{

    LCD_DPin |= (1<<LCD_DATA0_PIN)|(1<<LCD_DATA1_PIN)|(1<<LCD_DATA2_PIN)|(1<<LCD_DATA3_PIN)|(1<<RSPIN)|(1<<ENPIN);		//Kontroles LCD isvadai (D4-D7)
	_delay_ms(16);		// Laukiama daugiau nei 15 ms nuo pradzios
  
                            
	LCD_Action(0x02);	   
	LCD_Action(0x28);       
	LCD_Action(0x0C);       
	LCD_Action(0x06);       
	LCD_Action(0x01);       
	_delay_ms(5);           // Laukiama 5 ms
}


void LCD_Action( unsigned char cmnd )
{   
	LCD_Port = (LCD_Port & 0x0F) | (cmnd & 0xF0); 
	LCD_Port &= ~ (1<<RSPIN); 
	LCD_Port |= (1<<ENPIN); 
	_delay_us(1); 
	LCD_Port &= ~ (1<<ENPIN); 
	_delay_us(200);
	LCD_Port = (LCD_Port & 0x0F) | (cmnd << 4); 
	LCD_Port |= (1<<ENPIN); 
	_delay_us(1);  
	LCD_Port &= ~ (1<<ENPIN); 
	_delay_ms(2);  
}


void LCD_Clear()
{
	LCD_Action (0x01);		
	_delay_ms(2);			
	LCD_Action (0x80);		
}


void LCD_Print (char *str)
{
	int i;
	for(i=0; str[i]!=0; i++) 
	{
		LCD_Port = (LCD_Port & 0x0F) | (str[i] & 0xF0); 
		LCD_Port |= (1<<RSPIN); 
		LCD_Port|= (1<<ENPIN); 
		_delay_us(1); 
		LCD_Port &= ~ (1<<ENPIN); 
		_delay_us(200); 
		LCD_Port = (LCD_Port & 0x0F) | (str[i] << 4);
		LCD_Port |= (1<<ENPIN);
		_delay_us(1); 
		LCD_Port &= ~ (1<<ENPIN); 
		_delay_ms(2);
	}
}


void LCD_Printpos (char row, char pos, char *str)
{
	if (row == 0 && pos<16)
	LCD_Action((pos & 0x0F)|0x80); 
	else if (row == 1 && pos<16)
	LCD_Action((pos & 0x0F)|0xC0); 
	LCD_Print(str); 
}


void InitADC()
{

 ADMUX |= (1<<REFS0)|(1<<ADLAR); 
  
 
 ADCSRA |= (1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0)|(1<<ADEN);    
}

uint16_t ReadADC(uint8_t ADCchannel){

 ADMUX = (ADMUX & 0xF0) | (ADCchannel & 0x0F);

 ADCSRA |= (1<<ADSC);

 while( ADCSRA & (1<<ADSC) );

 return ADCH;
}


int main()
{

 Serial.begin(9600);
  
   InitADC();

  
	LCD_Init();			

	LCD_Printpos(0, 0, "VilniusTech Menu");	
	LCD_Action(0xC0);	

  while(1) {
      double sensorInput = ReadADC(0); 
  temp = (double)sensorInput / 256;  
 //temp = (double)sensorInput / 1024; 
    temp = temp * 5; 
  temp = temp - 0.5; 
  temp = temp * 100; 
  
    Serial.print("Temperatura: ");
    Serial.println(temp);// 
    //char buf[50];
    LCD_Printpos(1, 0, "Temp1: "); // Atvaizduojama simboliu seka 2 eiluteje
     
  dtostrf(temp, 6, 2, buf);
    LCD_Print(buf); 
    LCD_Print(" oC"); 
    _delay_ms(250); 
  }
	}